Kubernetes nodes
================
Добавляет ноды в кластер Kubernetes.  
Команды для подключения находятся в файлах каталога *files*.  
Содержимое этих файлов является результатом выполнения команды *kubeadm init*.  

Импорт роли
------------
```yaml
- name: k8s_nodes  
  src: https://gitlab.com/ansible_roles_v/k8s-nodes/  
  version: main  
```

Пример использования
--------------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - k8s_nodes
```